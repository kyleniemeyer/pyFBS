Development Lead
----------------

* Tomaž Bregar 

* Ahmed El Mahmoudi

* Miha Kodrič

* Domen Ocepek

* Francesco Trainotti

* Miha Pogačar

* Mert Göldeli

Contributors
------------

For a full list of `contributors`_ check the repository.

.. _contributors: https://gitlab.com/pyFBS/pyFBS/-/graphs/master
