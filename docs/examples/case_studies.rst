==============================
Case studies
==============================
These examples show applications of the pyFBS on more complex problems. Explore this application examples to see how pyFBS can be used on more complex dynamic problems.

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Operational Deflection Shapes">

.. only:: html

    .. figure:: ./data/ods.gif	   
       :target: ./case_studies/06_ODS.html

       Operational Deflection Shapes

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./case_studies/06_ODS


  
 
.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Experimental Modal Analysis">

.. only:: html

    .. figure:: ./data/modal_2_min.gif
       :target: ./case_studies/09_EMA.html

       Experimental Modal Analysis

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./case_studies/09_EMA
   

  
 
.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Transmission Simulator">

.. only:: html

    .. figure:: ./data/ten_display_four.png   
       :target: ./case_studies/10_TS.html

       Transmission Simulator

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./case_studies/10_TS