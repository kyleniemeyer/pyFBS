======================
Transfer Path Analysis
======================
Transfer-path analysis (TPA) is a reliable and effective diagnostic tool for the characterization of actively vibrating components and the propagation of noise and vibrations to the connected passive substructures [1]_. 
TPA offers the ability to analyse the vibration transfer between the individual components of the assembly, distinguish the partial transfer-path contribution and predict the receiver's response.

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Clasiccal TPA">

.. only:: html

    .. figure:: ./data/classical_tpa.svg   
       :target: ./tpa/classical_tpa.html

       Clasiccal TPA

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./tpa/classical_tpa





.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Component-based TPA">

.. only:: html

    .. figure:: ./data/component-based_tpa.svg   
       :target: ./tpa/component-based_tpa.html

       Component-based TPA

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./tpa/component-based_tpa





.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Transmssibility-based TPA">

.. only:: html

    .. figure:: ./data/otpa.svg   
       :target: ./tpa/transmissibility-based_tpa.html

       Transmissibility-based TPA

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./tpa/transmissibility-based_tpa


.. rubric:: References

.. [1] Maarten V. van der Seijs, Dennis de Klerk, and Daniel J. Rixen. General framework for transfer path analysis: history, theory and classification of techniques. Mechanical Systems and Signal Processing, 68-69:217–244, February 2016.