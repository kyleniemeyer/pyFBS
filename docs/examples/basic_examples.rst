Basic examples
==============

This examples show how to use basic features of pyFBS. Explore this basic examples to get familiar with the pyFBS workflow.

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Static display">

.. only:: html

    .. figure:: ./data/interaction.gif	   
       :target: ./basic_examples/01_static_display.html

       Static display

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./basic_examples/01_static_display


  
 
.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Interactive positioning">

.. only:: html

    .. figure:: ./data/snapping.gif	   
       :target: ./basic_examples/02_interactive_display.html

       Interactive positioning

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./basic_examples/02_interactive_display
   


   
.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="FRF synthetization">

.. only:: html

    .. figure:: ./data/FRF_syn-FRF-visualization.svg 
       :target: ./basic_examples/03_FRF_synthetization.html

       FRF synthetization

.. raw:: html

    </div>

.. toctree::
   :hidden:

   ./basic_examples/03_FRF_synthetization