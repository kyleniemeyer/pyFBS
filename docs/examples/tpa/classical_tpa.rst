====================
Classical TPA
====================
Classic TPA methods describe source excitations in terms of the interface forces between active and passive side. 
It is mainly used to troubleshoot NVH problems in existing products.

   
.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Matrix Inverse">

.. only:: html

    .. figure:: ./../data/matrix_inverse.png   
       :target: 14_matrix_inverse_TPA.html

       Matrix Inverse

.. raw:: html

    </div>

.. toctree::
   :hidden:

   14_matrix_inverse_TPA