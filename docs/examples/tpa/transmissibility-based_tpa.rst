==========================
Transmissibility-based TPA
==========================
The family of transmissibility-based TPA determines sound and vibration transfer from transmissibilities between sensors while assembly is subjected to operational excitation. 
This approach is convinient when TPA is used solely to identify the dominant path contributions in existing products. 

   
.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Operational TPA">

.. only:: html

    .. figure:: ./../data/labels.png   
       :target: 15_operational_TPA.html

       Operational TPA

.. raw:: html

    </div>

.. toctree::
   :hidden:

   15_operational_TPA